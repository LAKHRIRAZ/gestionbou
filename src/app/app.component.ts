import { Component, Renderer, ElementRef } from '@angular/core';
import { Login } from './login';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { AuthserviceService } from './core/service/service auth/authservice.service';
import { MatDialog, MatSnackBar } from '@angular/material';
import { User } from 'src/user';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'gesboutique';
  formGroup:FormGroup;
 
  formauser:User[];
  user:User[];

  IduserNavigation: {
    idUser:number
    nomUser:string
    prenomUser:string
    usser:string
    passeword:string
}

  userLogin: Login;
  constructor(private _snackBar: MatSnackBar,private el: ElementRef, private rendere: Renderer, private auth: AuthserviceService,
    private route: Router, private http: HttpClient, private builder: FormBuilder, public dialog: MatDialog) { }

    ngOnInit(): void {
      
  
      this.http.get('http://localhost:51026/api/logins/lastLogin').subscribe(
          res => {
          var login: Login;
          login = res[0] as Login;
          if (login.iduserNavigation.nomUser == "admin") {
            this.route.navigate(['signup']);
          }
          if (login != null) {
            this.userLogin = login;
            if (login.iduserNavigation.nomUser == "visiteur") {
              document.getElementById('nav').style.display = "";
            }
          } else {
            if (login == null) {
             this.route.navigate(['signin']);
            }
          }
        },
        err => {
  
        }
      );
  
   
    }


    logOut() {


      this.http.get('http://localhost:51026/api/logins/lastLogin').subscribe(
         res => {
          var login: Login;
          login = res[0] as Login;
          if (login != null) {
            this.userLogin = login;
            var formLog: FormGroup;
            formLog = this.builder.group({
              id: new FormControl(this.userLogin.id),
              idUser: new FormControl(this.userLogin.iduser),
              logOut: new FormControl('true'),
              DateLogin: new FormControl('2019-01-11'),
              DateLogout: new FormControl('2020-01-11'),
            });
            var Login: Login = formLog.value;
            console.log('logiiiiin', Login);
            this.http.put('http://localhost:51026/api/logins/' + this.userLogin.id, Login).subscribe(
               res => {
                console.log('logout avec secsus');
                this._snackBar.open("User Deconnecte","A La Prochaine !",
                {
                  duration: 2500,
                  
                });
                this.route.navigate(['signin']);
              }, err => {
                console.log('not logout', err);
              }
            );
            if (login.iduserNavigation.nomUser == "admin") {
              document.getElementById('nav').style.display = "";
            }
          } else {
            if (login == null) {
              // document.getElementById('nav').style.display = "none";
              this.route.navigate(['signin']);
            }
          }
        },
        err => {
  
        }
      );
    }
  
  }