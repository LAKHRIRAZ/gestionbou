import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { User } from 'src/user';
import { MatPaginator, MatTableDataSource, MatDialog, MatSnackBar } from '@angular/material';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { UserserviceService } from 'src/app/core/service/service User/userservice.service';
import { AjouteruserComponent } from 'src/app/compenent/user/add user/ajouteruser/ajouteruser.component';
import { ModifieruserComponent } from 'src/app/compenent/user/modifier user/modifieruser/modifieruser.component';
import { Login } from 'src/app/login';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  formGroup:FormGroup;
 
  formauser:User[];
  user:User[];
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  constructor( private builder: FormBuilder,private http: HttpClient, private route: Router,private userservice :UserserviceService, public dialog: MatDialog,private _snackBar: MatSnackBar) {  }
  datasource:MatTableDataSource <any>;
  displayedColumns: string[] = ['idUser','nomUser','prenomUser','usser','passeword','supprimer','modifier'];


  ngOnInit() {
    this.userservice.getAllUsers().subscribe(res=>
  { this.datasource=new MatTableDataSource(res as User[]);
    this.datasource.paginator = this.paginator;
  console.log(this.datasource);
  }
    );

  }
  suppelement(idUser)
  {
     var idu=parseInt(idUser);
     console.log('idUser=',idUser)
     this.userservice.deleteUsers(idu).subscribe(res=>
      {
        this.ngOnInit();
      });
      // window.alert('Le client a ete supprime')
      this._snackBar.open("user est supprimer","!",
      {
        duration: 2500,
        
      });
     
  }

  
  openDialog1(elt): void {
    
    const dialogRef = this.dialog.open(AjouteruserComponent, {
     
  width: '600px',
      data: elt
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      
      this.ngOnInit()
    });
 
  }

  openDialog(elt): void {
    
    const dialogRef = this.dialog.open(ModifieruserComponent, {
      width: '600px',
      data: elt
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      
      this.ngOnInit()
    });
 
  }
  applyFilter(filterValue: string) {
    this.datasource.filter = filterValue.trim().toLowerCase();
  }

  userLogin: Login;

  logOut() {


    this.http.get('http://localhost:51026/api/logins/lastLogin').subscribe(
       res => {
        var login: Login;
        
        login = res[0] as Login;
        if (login != null) {
          this.userLogin = login;
          var formLog: FormGroup;
          formLog = this.builder.group({
            id: new FormControl(this.userLogin.id),
            idUser: new FormControl(this.userLogin.iduser),
            logOut: new FormControl('true'),
            DateLogin: new FormControl('2019-01-11'),
            DateLogout: new FormControl('2020-01-1'),
          });
          var Login: Login = formLog.value;
          console.log('logiiiiin', Login);
          this.http.put('http://localhost:51026/api/logins/' + this.userLogin.id, Login).subscribe(
             res => {
              console.log('logout avec secsus');
              this._snackBar.open("User Deconnecte","A La Prochaine !",
              {
                duration: 2500,
                
              });
            
              this.route.navigate(['signin']);
            }, err => {
              console.log('not logout', err);
            }
          );
          if (login.iduserNavigation.nomUser == "admin") {
            document.getElementById('nav').style.display = "";
          }
        } else {
          if (login == null) {
            // document.getElementById('nav').style.display = "none";
            this.route.navigate(['signin']);
          }
        }
      },
      err => {

      }
    );
  }


}
