import { Component, OnInit } from '@angular/core';
import { UserserviceService } from 'src/app/core/service/service User/userservice.service';
import { Router } from '@angular/router';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { User } from 'src/user';
import { Login } from 'src/app/login';
import { AuthserviceService } from 'src/app/core/service/service auth/authservice.service';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-authentification',
  templateUrl: './authentification.component.html',
  styleUrls: ['./authentification.component.css']
})
export class AuthentificationComponent implements OnInit {
  formGroup: FormGroup;
  formauser:User[];
  user:User[];
 
  constructor(private router: Router, private auth: AuthserviceService, private http: HttpClient,
    private builder: FormBuilder,private _snackBar: MatSnackBar ) { }

  ngOnInit() {
    document.getElementById('nav').style.display = "none";
    this.formGroup = this.builder.group({
      usser: new FormControl(""),
      passeword: new FormControl("")
    });
  }

  signin() {
    this.auth.getUser(this.formGroup.controls.usser.value, this.formGroup.controls.passeword.value).subscribe(
      res => {
        var userConnect: User = res as User;
        var formLog: FormGroup;
        formLog = this.builder.group({
          id: new FormControl(0),
          iduser: new FormControl(userConnect.idUser),
          logOut: new FormControl('false'),
          dateLogin: new FormControl('2019-01-11'),
          dateLogout: new FormControl(),
        });
        var Login: Login = formLog.value;
        this.http.post('http://localhost:51026/api/logins', Login).subscribe(
          
          result => {
            this._snackBar.open("User Connecte","Bienvenu !",
            {
              duration: 3000,
              
            });
            console.log('ajoute login', result);
            document.getElementById('nav').style.display = "none";
            var user: User;
            user = res as User;
            if (user.nomUser == "admin") {
              this.router.navigate(['signup']);
              document.getElementById('nav').style.display = "none";
            } else if (user.nomUser == "visiteur") {
              document.getElementById('nav').style.display = "";
              this.router.navigate(['Espace Client']);
            }
          }
        );

      },
      err => {
        console.log('err', err);
        this._snackBar.open("User ou Passeword incorrecte","Ressayer !",
        {
          duration: 2500,
          
        });
      }
    );
  }


  
  }

