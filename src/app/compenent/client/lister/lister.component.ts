import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { ClientserviceService } from 'src/app/core/service/service Client/clientservice.service';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { MatDialog, MatTableDataSource, MatPaginator, MAT_DIALOG_DATA } from '@angular/material';
import { Commande } from 'src/commande';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-lister',
  templateUrl: './lister.component.html',
  styleUrls: ['./lister.component.css']
})
export class ListerComponent implements OnInit {
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  constructor( @Inject(MAT_DIALOG_DATA) public data:any ,private route: ActivatedRoute,private clientservice :ClientserviceService, public dialog: MatDialog,private Builder:FormBuilder)
    { }
    formee:FormGroup;
  datasource
  displayedColumns: string[] = ['idC','idCli','idp','idUeser','qte','dateDebutGar','dateFinGar'];
  idCli:Commande;
  ngOnInit() {
    this.formee=this.Builder.group(
      {
        
      }
    )
    
       var id :number = parseInt(this.data.idCli);
       console.log(id);
    this.clientservice.getcommande(id).subscribe(res=>
  { this.datasource=new MatTableDataSource(res as Commande[]);
    this.datasource.paginator = this.paginator;
  console.log(this.datasource);
  }
    );

  }
   // lister(idCli)
  // {
  //    var idc=parseInt(idCli);
  //    console.log('idCli=',idCli)
  //    this.clientservice.getcommande(idc).subscribe(res=>
  //     {
  //       this.ngOnInit();
  //     });
  //     // window.alert('Le client a ete supprime')
      
     
  // }

}
