import { Component, OnInit, ViewChild } from '@angular/core';
import { ClientserviceService } from 'src/app/core/service/service Client/clientservice.service';
import { MatDialog, MatTableDataSource, MatPaginator, MatSnackBar } from '@angular/material';
import { Client } from 'src/client';
import { AjouterclientComponent } from '../../add client/ajouterclient/ajouterclient.component';
import { ModifierclientComponent } from '../../modifier client/modifierclient/modifierclient.component';
import { ExcelDataType } from 'xlsx/types';
import { ExcelserviceService } from 'src/app/core/service/service Exporte/excelservice.service';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { ListerComponent } from '../../lister/lister.component';

@Component({
  selector: 'app-listerclient',
  templateUrl: './listerclient.component.html',
  styleUrls: ['./listerclient.component.css']
})
export class ListerclientComponent implements OnInit {
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  constructor(private clientservice :ClientserviceService, public dialog: MatDialog,private _snackBar: MatSnackBar) {  }
  datasource:MatTableDataSource <any>;
  displayedColumns: string[] = ['idCli','nomCli','prenomCli','cin','supprimer','modifier','lister'];


  ngOnInit() {
    this.clientservice.getAllClient().subscribe(res=>
  { this.datasource=new MatTableDataSource(res as Client[]);
    this.datasource.paginator = this.paginator;
  console.log(this.datasource);
  }
    );

  }
  suppelement(idCli)
  {
     var idc=parseInt(idCli);
     console.log('idCli=',idCli)
     this.clientservice.deleteclient(idc).subscribe(res=>
      {
        this.ngOnInit();
      });
      // window.alert('Le client a ete supprime')
      this._snackBar.open("le client est supprimer","!",
      {
        duration: 2500,
        
      });
     
  }

  openDialog1(elt): void {
    
    const dialogRef = this.dialog.open(AjouterclientComponent, {
      width: '600px',
      data: elt
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      
      this.ngOnInit()
    });
 
  }
   openDialog(elt): void {
    
    const dialogRef = this.dialog.open(ModifierclientComponent, {
      width:  '600px',
      data: elt
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      
      this.ngOnInit()
    });
 
  }
  applyFilter(filterValue: string) {
    this.datasource.filter = filterValue.trim().toLowerCase();
  }

 

  openDialog2(elt): void {
    
    const dialogRef = this.dialog.open(ListerComponent, {
      width: '700px',
      data: elt
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      
      this.ngOnInit()
    });
 
  }
 
 
}
