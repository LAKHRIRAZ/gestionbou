import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { Client } from 'src/client';
import { ClientserviceService } from 'src/app/core/service/service Client/clientservice.service';
import { MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-ajouterclient',
  templateUrl: './ajouterclient.component.html',
  styleUrls: ['./ajouterclient.component.css']
})
export class AjouterclientComponent implements OnInit {

  formaclient:Client;
  formee:FormGroup;
  
  constructor(private clientservice : ClientserviceService ,private Builder:FormBuilder,
   public dialogRef: MatDialogRef<AjouterclientComponent>,private _snackBar: MatSnackBar )
     { }

  
  ngOnInit() {
    this.formee=this.Builder.group(
      {
        idCli:new FormControl(0),
        nomCli:new FormControl(),
        prenomCli:new FormControl(),
        cin:new FormControl(),
      }
    )
  
    }
    submit(){
    
      this.formaclient=this.formee.value;
        this.clientservice.postclient(this.formaclient).subscribe(res=>{
         
          // window.alert('bien ajoute');
          this._snackBar.open("bien ajoute","!",
          {
            duration: 2500,
            
          });
     
            this.dialogRef.close()
        
          this.formee=this.Builder.group(
            {
              idCli:new FormControl(0),
              nomCli:new FormControl(),
              prenomCli:new FormControl(),
              cin:new FormControl()
            }
          
          
          )},
        
        
        err=>{
          console.log(err)
          
        })
        
      } 

      reset()
    {
      this.formee=this.Builder.group(
        {
          idCli:new FormControl(0),
          nomCli:new FormControl(),
          prenomCli:new FormControl(),
          cin:new FormControl()
        }
      )
  
    }
    onNoClick(): void {
     
      this.dialogRef.close()
    }
}
