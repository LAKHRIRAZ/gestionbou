import { Component, OnInit, Inject } from '@angular/core';
import { ClientserviceService } from 'src/app/core/service/service Client/clientservice.service';
import { FormBuilder } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-modifierclient',
  templateUrl: './modifierclient.component.html',
  styleUrls: ['./modifierclient.component.css']
})
export class ModifierclientComponent implements OnInit {

  formee: any;
  formaclient: any;
  constructor(private clientservice :ClientserviceService ,private Builder:FormBuilder ,
    @Inject(MAT_DIALOG_DATA) public data:any, public dialogRef: MatDialogRef<ModifierclientComponent>,private _snackBar: MatSnackBar ) { }


    ngOnInit() {
      console.log(this.data);
      this.formee=this.Builder.group(
        {
          idCli:[this.data.idCli],
          nomCli:[this.data.nomCli],
          prenomCli:[this.data.prenomCli],
          cin:[this.data.cin],
         
        }
       
      )
      console.log(this.data.idCli);
    }
    get f(){
      return this.formee.controls
    }
      onNoClick(): void {
     
        this.dialogRef.close()
      }
      submit()
      {
        
         this.formaclient=this.formee.value;
         console.log(this.formaclient);
         this.clientservice.puttclient(this.data.idCli,this.formaclient).subscribe(res=>
          {
            this._snackBar.open("Client Modifier","!",
          {
            duration: 2500,
            
          });
            this.onNoClick();
          });
         
      }


}
