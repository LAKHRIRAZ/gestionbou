import { Component, OnInit } from '@angular/core';
import { Client } from 'src/client';
import { Produit } from 'src/produit';
import { User } from 'src/user';
import { CommandeserviceService } from 'src/app/core/service/servie Commande/commandeservice.service';
import { ClientserviceService } from 'src/app/core/service/service Client/clientservice.service';
import { ServiceproduitService } from 'src/app/core/service/service Produit/serviceproduit.service';
import { UserserviceService } from 'src/app/core/service/service User/userservice.service';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { Commande } from 'src/commande';
import * as moment from 'moment';
import { MatDialogRef, MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-ajoutercommande',
  templateUrl: './ajoutercommande.component.html',
  styleUrls: ['./ajoutercommande.component.css']
})
export class AjoutercommandeComponent implements OnInit {
  options: Client[];
  optionss: Produit[];
  optionsss:User[]
  forme:FormGroup;
 
  formacommande:Commande;
  constructor(private _snackBar: MatSnackBar,private Commandeservice:CommandeserviceService,private clientservice : ClientserviceService,private produitservice:ServiceproduitService,private Userservice:UserserviceService ,private Builder:FormBuilder,
    public dialogRef: MatDialogRef<AjoutercommandeComponent>) { }

  ngOnInit() {
    this.clientservice.getAllClient().subscribe(
      res=>{
        this.options=res as Client[];
      },
      err=>{
        console.log(err);
      }
    )


    this.produitservice.getAllProduit().subscribe(
      res=>{
        this.optionss=res as Produit[];
      },
      err=>{
        console.log(err);
      }
    )

    this.Userservice.getAllUsers().subscribe(
      res=>{
        this.optionsss=res as User[];
      },
      err=>{
        console.log(err);
      }
    )
    

   
    this.forme=this.Builder.group(
      {
        idC:new FormControl(0),
        idCli:new FormControl(),
        idp:new FormControl(),
        idUeser:new FormControl(),
        qte:new FormControl(0),
        dateDebutGar:new FormControl(),
        dateFinGar:new FormControl()
      }
    )

    
  }

  submit(){
  
    var d:string=moment(this.forme.controls.dateDebutGar.value).format('YYYY-MM-DD');
    this.forme.controls.dateDebutGar.patchValue(d);

    var d1:string=moment(this.forme.controls.dateFinGar.value).format('YYYY-MM-DD');
    this.forme.controls.dateFinGar.patchValue(d1);

    console.log(d,d1);

    this.formacommande=this.forme.value;
    console.log(this.formacommande);
      this.Commandeservice.postCommande(this.formacommande).subscribe(res=>{
       
        console.log('bien ajoute',res);
        // window.alert("la Commande a ete bien effectuer")
        this._snackBar.open("bien ajoute","!",
        {
          duration: 2500,
          
        });
        this.dialogRef.close()
        
   
    this.forme=this.Builder.group(
      {
        idC:new FormControl(0),
        idCli:new FormControl(),
        idp:new FormControl(),
        idUeser:new FormControl(),
        qte:new FormControl(0),
        dateDebutGar:new FormControl(),
        dateFinGar:new FormControl()
      }
    )
        
      },
      err=>{
        console.log(err)
        
      })
    }
  
    onNoClick(): void {
     
      this.dialogRef.close()
    }
}
