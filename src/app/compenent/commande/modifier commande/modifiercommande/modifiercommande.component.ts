import { Component, OnInit, Inject } from '@angular/core';
import { CommandeserviceService } from 'src/app/core/service/servie Commande/commandeservice.service';
import { ModifierclientComponent } from 'src/app/compenent/client/modifier client/modifierclient/modifierclient.component';
import { MAT_DIALOG_DATA, MatDialogRef, MatSnackBar } from '@angular/material';
import { FormBuilder, FormControl } from '@angular/forms';
import * as moment from 'moment';
import { ClientserviceService } from 'src/app/core/service/service Client/clientservice.service';
import { ServiceproduitService } from 'src/app/core/service/service Produit/serviceproduit.service';
import { UserserviceService } from 'src/app/core/service/service User/userservice.service';
import { Client } from 'src/client';
import { Produit } from 'src/produit';
import { User } from 'src/user';

@Component({
  selector: 'app-modifiercommande',
  templateUrl: './modifiercommande.component.html',
  styleUrls: ['./modifiercommande.component.css']
})
export class ModifiercommandeComponent implements OnInit {
  dateDebutGar:string;
  dateFinGar:string;
  formee: any;
  formacommande: any;
  options: Client[];
  optionss: Produit[];
  optionsss:User[]
  constructor(private Commandeservice:CommandeserviceService,private _snackBar: MatSnackBar,private clientservice : ClientserviceService,private produitservice:ServiceproduitService,private Userservice:UserserviceService ,private Builder:FormBuilder ,
    @Inject(MAT_DIALOG_DATA) public data:any, public dialogRef: MatDialogRef<ModifiercommandeComponent> ) { }

    ngOnInit() {

      console.log(this.data);
      this.formee=this.Builder.group(
        {
          idC:[this.data.idC],
          idCli:[this.data.idCli],
          idp:[this.data.idp],
          idUeser:[this.data.idUeser],
          qte:[this.data.qte],
          dateDebutGar:[this.data.dateDebutGar],
          dateFinGar:[this.data.dateFinGar],
          
        });
        
       
        // this.data.idCli=[this.data.idCliNavigation.idCli],
        // this.data.idp=[this.data.idpNavigation.idp],
        // this.data.idUeser=[this.data.idUeserNavigation.idUeser],
          
        
        
      console.log(this.data.idp);
    }
    get f(){
      return this.formee.controls
    }
      onNoClick(): void {
     
        this.dialogRef.close()
      }

      submit(){
  
        var d:string=moment(this.formee.controls.dateDebutGar.value).format('YYYY-MM-DD');
        this.formee.controls.dateDebutGar.patchValue(d);
    
        var d1:string=moment(this.formee.controls.dateFinGar.value).format('YYYY-MM-DD');
        this.formee.controls.dateFinGar.patchValue(d1);
    
        console.log(d,d1);
    
        this.formacommande=this.formee.value;
        console.log(this.formacommande);

          this.Commandeservice.puttCommande(this.data.idC,this.formacommande).subscribe(res=>
        { 
          // window.alert("la Commande est modifiée")
          this.onNoClick();
          this._snackBar.open("Commande Modifier","!",
          {
            duration: 2500,
            
          });
        });
           
           
            
       
        this.formee=this.Builder.group(
          {
            idC:new FormControl(0),
            idCli:new FormControl(),
            idp:new FormControl(),
            idUeser:new FormControl(),
            qte:new FormControl(0),
            dateDebutGar:new FormControl(),
            dateFinGar:new FormControl()
          }
        )
            
        }
        

}
