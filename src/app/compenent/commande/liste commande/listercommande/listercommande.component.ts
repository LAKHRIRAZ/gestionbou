import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatDialog, MatSnackBar, MatPaginator } from '@angular/material';
import { CommandeserviceService } from 'src/app/core/service/servie Commande/commandeservice.service';
import { Commande } from 'src/commande';
import { ModifiercommandeComponent } from '../../modifier commande/modifiercommande/modifiercommande.component';
import { AjoutercommandeComponent } from '../../add commande/ajoutercommande/ajoutercommande.component';
import { FormGroup, FormControl,FormBuilder } from '@angular/forms';
import * as moment from 'moment';
import { HttpClient } from '@angular/common/http';
import { ExcelserviceService } from 'src/app/core/service/service Exporte/excelservice.service';
import { Observable } from 'rxjs';



@Component({
  selector: 'app-listercommande',
  templateUrl: './listercommande.component.html',
  styleUrls: ['./listercommande.component.css']
})
export class ListercommandeComponent implements OnInit {
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  constructor(private commandeservice :CommandeserviceService ,private _snackBar: MatSnackBar,public dialog: MatDialog,private Builder:FormBuilder,private excelService: ExcelserviceService,
     private http: HttpClient) 
    {   
    //   this.getJSON().subscribe(data => 
    //   {
    //   data.forEach(row => {
    //     this.excel.push(row);
    //     console.log("row",row);
    //   });
    // });
       
    }
  datasource:MatTableDataSource <any>;
  displayedColumns: string[] = ['idC','idCli','idp','idUeser','qte','dateDebutGar','dateFinGar','supprimer','modifier'];

  formacommande:Commande[];
  formee:FormGroup;
  dateDebutGar=   new FormControl(moment(new Date()).format('YYYY-MM-DD'+'T00:00:00'));
  dateFinGar=  new FormControl(moment(new Date()).format('YYYY-MM-DD'+'T00:00:00'));
  qte= new FormControl();
  de;
  ds
  url:string="http://localhost:51026/api/commandes";
  data=[];
  ngOnInit() {
   
    this.commandeservice.getAllCommande().subscribe(res=>
  { this.datasource=new MatTableDataSource(res as Commande[]);
    this.datasource.paginator = this.paginator;
  console.log(this.datasource);
  }
    );
    this.formee=this.Builder.group(
      {
        qte:new FormControl(0),
        dateDebutGar:new FormControl(),
        dateFinGar:new FormControl(),
      }
    )
    this.formacommande=this.formee.value;
    this.de = moment(this.dateDebutGar.value).format('YYYY-MM-DD' + 'T00:00:00');
    this.ds = moment(this.dateFinGar.value).format('YYYY-MM-DD' + 'T00:00:00');

   var d1=moment(new Date()).format('YYYY-MM-DD');
   this.dateFinGar.patchValue(d1);
   this.dateDebutGar.patchValue(d);
   var d=moment(new Date()).format('YYYY-MM-DD');

  }
  suppelement(idC)
  {
     var idc=parseInt(idC);
     console.log('idC=',idC)
     this.commandeservice.deleteCommande(idc).subscribe(res=>
      {
        this.ngOnInit();
      });
      // window.alert('La Commande a ete supprime')
      this._snackBar.open("Commande supprimer","!",
          {
            duration: 2500,
            
          });
  }

  openDialog1(elt): void {
    
    const dialogRef = this.dialog.open(AjoutercommandeComponent, {
    
      width: '600px',
      data: elt,
     
      
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      
      this.ngOnInit()
    });
 
  }
   openDialog(elt): void {
    
    const dialogRef = this.dialog.open(ModifiercommandeComponent, {
      width: '40%',
      data: elt
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      
      this.ngOnInit()
    });
  }

 

  applyFilter(filterValue: string) {
    this.datasource.filter = filterValue.trim().toLowerCase();
  }
  onSearch() {
    this.formacommande=this.formee.value;

    var d:string=moment(this.formee.controls.dateDebutGar.value).format('YYYY-MM-DD');
    this.formee.controls.dateDebutGar.patchValue(d);

    var d1:string=moment(this.formee.controls.dateFinGar.value).format('YYYY-MM-DD');
    this.formee.controls.dateFinGar.patchValue(d1);

     if (this.formee.controls.dateDebutGar.value!= null && this.formee.controls.dateFinGar.value != null) {
      var d = moment(this.formee.controls.dateDebutGar.value).format('YYYY-MM-DD');
      var d1 = moment(this.formee.controls.dateFinGar.value).format('YYYY-MM-DD');
      this.commandeservice.getgarentidate(d, d1).subscribe(
        res => {
           this.formacommande = res as Commande[];
          console.log('element recherchee',this.formacommande);
          this.datasource=new MatTableDataSource(res as Commande[]);
      
        },
        errr=>{
          console.log(errr);
        });
    } 
  }

  excel= [];

  exportAsXLSX(): void {

      
      this.getJSON().subscribe(data => 
      {
      data.forEach(row => {
        this.excel.push(row);
        console.log("row",row);
      });
    });
   
   
    this.formacommande=this.formee.value;

    var d:string =moment(this.formee.controls.dateDebutGar.value).format('YYYY-MM-DD' );
    this.formee.controls.dateDebutGar.patchValue(d);

    var d1:string =moment(this.formee.controls.dateFinGar.value).format('YYYY-MM-DD' );
    this.formee.controls.dateDebutGar.patchValue(d1);
    console.log(',',d);
    console.log(',',d1);

    console.log('excel :',this.excel);
    
         this.excelService.exportAsExcelFile(this.excel, 'test');
      
  }

  
   public getJSON(): Observable<any> {
 
   


    this.formacommande=this.formee.value;

    var d =moment(this.formee.controls.dateDebutGar.value).format('YYYY-MM-DD');
    this.formee.controls.dateDebutGar.patchValue(d);

    var d1 =moment(this.formee.controls.dateFinGar.value).format('YYYY-MM-DD' );

    this.formee.controls.dateDebutGar.patchValue(d1);
    return this.http.get('http://localhost:51026/api/commandes/export/'+d+'/'+d1);
   

    // if (this.dateDebutGar.value != null && this.dateFinGar.value != null)
    // {
    //   var d = moment(this.formee.controls.dateDebutGar.value).format('YYYY-MM-DD' );
    //   var d1 = moment(this.formee.controls.dateFinGar.value).format('YYYY-MM-DD');
     
    //   return this.http.get('http://localhost:51026/api/commandes/export/'+ d + '/' + d1);
    // } else {
    //   return this.http.get('http://localhost:51026/api/commandes');
    // }
  }


    
   
  }



