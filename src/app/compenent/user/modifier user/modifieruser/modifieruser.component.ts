import { Component, OnInit, Inject } from '@angular/core';
import { MatSnackBar, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormBuilder } from '@angular/forms';
import { UserserviceService } from 'src/app/core/service/service User/userservice.service';

@Component({
  selector: 'app-modifieruser',
  templateUrl: './modifieruser.component.html',
  styleUrls: ['./modifieruser.component.css']
})
export class ModifieruserComponent implements OnInit {

  formee: any;
  formauser: any;
  constructor(private userservice :UserserviceService ,private Builder:FormBuilder ,
    @Inject(MAT_DIALOG_DATA) public data:any, public dialogRef: MatDialogRef<ModifieruserComponent>,private _snackBar: MatSnackBar ) { }


  
    ngOnInit() {
      console.log(this.data);
      this.formee=this.Builder.group(
        {
          idUser:[this.data.idUser],
          nomUser:[this.data.nomUser],
          prenomUser:[this.data.prenomUser],
          usser:[this.data.usser],
          passeword:[this.data.passeword],
         
        }
       
      )
      console.log(this.data.idUser);
    }
    get f(){
      return this.formee.controls
    }
    onNoClick(): void {
     
      this.dialogRef.close()
    }

    submit()
    {
      
       this.formauser=this.formee.value;
       console.log(this.formauser);
       this.userservice.puttUsers(this.data.idUser,this.formauser).subscribe(res=>
        {
          this._snackBar.open("User Modifier","!",
        {
          duration: 2500,
          
        });
          this.onNoClick();
        });
       
    }

}
