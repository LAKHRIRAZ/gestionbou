import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ImprimeruserComponent } from './imprimeruser.component';

describe('ImprimeruserComponent', () => {
  let component: ImprimeruserComponent;
  let fixture: ComponentFixture<ImprimeruserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ImprimeruserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ImprimeruserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
