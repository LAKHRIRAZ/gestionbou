import { Component, OnInit, ViewChild } from '@angular/core';
import * as jspdf from 'jspdf';
import html2canvas from 'html2canvas';
import { UserserviceService } from 'src/app/core/service/service User/userservice.service';
import { MatDialog, MatSnackBar, MatTableDataSource, MatPaginator } from '@angular/material';
import { User } from 'src/user';
@Component({
  selector: 'app-imprimeruser',
  templateUrl: './imprimeruser.component.html',
  styleUrls: ['./imprimeruser.component.css']
})
export class ImprimeruserComponent implements OnInit {
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  constructor(private userservice :UserserviceService, public dialog: MatDialog,private _snackBar: MatSnackBar) {  }
  datasource:MatTableDataSource <any>;
  displayedColumns: string[] = ['idUser','nomUser','prenomUser','usser','passeword',];

  ngOnInit() {
    this.userservice.getAllUsers().subscribe(res=>
  { this.datasource=new MatTableDataSource(res as User[]);
  
  console.log(this.datasource);
  }
    );

  }
  public generatePDF() {
    var data = document.getElementById('contentToConvert');
    html2canvas(data).then(canvas => {
      // Few necessary setting options 
      // var imgWidth = 208;
      var pageHeight = 280;
      var imgWidth = 208;
      var imgHeight = canvas.height * imgWidth / canvas.width;
      var heightLeft = imgHeight;

      const contentDataURL = canvas.toDataURL('image/png')
      let pdf = new jspdf('p', 'mm', 'a4'); // A4 size page of PDF 
      var position = 0;
      pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight)
      pdf.save('.pdf'); 
         });
  }
}
