import { Component, OnInit } from '@angular/core';
import { User } from 'src/user';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { UserserviceService } from 'src/app/core/service/service User/userservice.service';
import { MatSnackBar, MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-ajouteruser',
  templateUrl: './ajouteruser.component.html',
  styleUrls: ['./ajouteruser.component.css']
})
export class AjouteruserComponent implements OnInit {

  formauser:User;
  formee:FormGroup;
  
  constructor(private clientservice : UserserviceService ,private Builder:FormBuilder,
   public dialogRef: MatDialogRef<AjouteruserComponent>,private _snackBar: MatSnackBar )
   {}
 
   options: string[] = ['admin', 'visiteur'];

   ngOnInit() {
    this.formee=this.Builder.group(
      {
        idUser:new FormControl(0),
        nomUser:new FormControl(),
        prenomUser:new FormControl(),
        usser:new FormControl(),
        passeword:new FormControl(""),
      }
    )
  
  }

  submit(){
    
    this.formauser=this.formee.value;
      this.clientservice.postUsers(this.formauser).subscribe(res=>{
       
        // window.alert('bien ajoute');
        this._snackBar.open("bien ajoute","!",
        {
          duration: 2500,
          
        });
   
          this.dialogRef.close()
      
        this.formee=this.Builder.group(
          {
            idUser:new FormControl(0),
            nomUser:new FormControl(),
            prenomUser:new FormControl(),
            usser:new FormControl(),
            passeword:new FormControl(),
          }
        )},
      err=>{
        console.log(err)
        
      })
      
    } 
    onNoClick(): void {
     
      this.dialogRef.close()
    }


}
