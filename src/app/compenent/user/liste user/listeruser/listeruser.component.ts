import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatDialog, MatSnackBar, MatTableDataSource } from '@angular/material';
import { UserserviceService } from 'src/app/core/service/service User/userservice.service';
import { User } from 'src/user';
import { AjouteruserComponent } from '../../add user/ajouteruser/ajouteruser.component';
import { ModifieruserComponent } from '../../modifier user/modifieruser/modifieruser.component';
import { ImprimeruserComponent } from '../../imprimer user/imprimeruser/imprimeruser.component';

@Component({
  selector: 'app-listeruser',
  templateUrl: './listeruser.component.html',
  styleUrls: ['./listeruser.component.css']
})
export class ListeruserComponent implements OnInit {
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  constructor(private userservice :UserserviceService, public dialog: MatDialog,private _snackBar: MatSnackBar) {  }
  datasource:MatTableDataSource <any>;
  displayedColumns: string[] = ['idUser','nomUser','prenomUser','usser','passeword'];

  ngOnInit() {
    this.userservice.getAllUsers().subscribe(res=>
  { this.datasource=new MatTableDataSource(res as User[]);
    this.datasource.paginator = this.paginator;
  console.log(this.datasource);
  }
    );

  }

  suppelement(idUser)
  {
     var idu=parseInt(idUser);
     console.log('idUser=',idUser)
     this.userservice.deleteUsers(idu).subscribe(res=>
      {
        this.ngOnInit();
      });
      // window.alert('Le client a ete supprime')
      this._snackBar.open("user est supprimer","!",
      {
        duration: 2500,
        
      });
     
  }

  openDialog1(elt): void {
    
    const dialogRef = this.dialog.open(AjouteruserComponent, {
     
  width: '600px',
      data: elt
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      
      this.ngOnInit()
    });
 
  }

  openDialog(elt): void {
    
    const dialogRef = this.dialog.open(ModifieruserComponent, {
      width: '600px',
      data: elt
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      
      this.ngOnInit()
    });
 
  }
  applyFilter(filterValue: string) {
    this.datasource.filter = filterValue.trim().toLowerCase();
  }

  openDialog2(elt): void {
    
    const dialogRef = this.dialog.open(ImprimeruserComponent, {
      width: '700px',
      data: elt
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      
      this.ngOnInit()
    });
 
  }

}
