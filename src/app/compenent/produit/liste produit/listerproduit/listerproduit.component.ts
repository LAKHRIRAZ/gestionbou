import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatDialog, MatTableDataSource, MatSnackBar } from '@angular/material';
import { ServiceproduitService } from 'src/app/core/service/service Produit/serviceproduit.service';
import { Produit } from 'src/produit';
import { AjouterproduitComponent } from '../../add produit/ajouterproduit/ajouterproduit.component';
import { ModifierproduitComponent } from '../../modifier produit/modifierproduit/modifierproduit.component';

@Component({
  selector: 'app-listerproduit',
  templateUrl: './listerproduit.component.html',
  styleUrls: ['./listerproduit.component.css']
})
export class ListerproduitComponent implements OnInit {

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  constructor(private _snackBar: MatSnackBar,private produitservice :ServiceproduitService, public dialog: MatDialog) { }
  datasource:MatTableDataSource <any>;
  displayedColumns: string[] = ['idp','nomp','ref','prix','supprimer','modifier'];


  ngOnInit() {
    this.produitservice.getAllProduit().subscribe(res=>
  { this.datasource=new MatTableDataSource(res as Produit[]);
    this.datasource.paginator = this.paginator;
  console.log(this.datasource);
  }
    );

  }
  suppelement(idp)
  {
     var id=parseInt(idp);
     console.log('idp=',idp)
     this.produitservice.deleteproduit(id).subscribe(res=>
      {
        this.ngOnInit();
      });
      this._snackBar.open("Produit supprimer","!",
              {
                duration: 2500,
                
              });
     
  }

  openDialog1(elt): void {
    
    const dialogRef = this.dialog.open(AjouterproduitComponent, {
      width: '600px',
      data: elt
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      
      this.ngOnInit()
    });
 
  }
   openDialog(elt): void {
    
    const dialogRef = this.dialog.open(ModifierproduitComponent, {
      width: '600px',
      data: elt
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      
      this.ngOnInit()
    });
 
  }
  applyFilter(filterValue: string) {
    this.datasource.filter = filterValue.trim().toLowerCase();
  }

}
