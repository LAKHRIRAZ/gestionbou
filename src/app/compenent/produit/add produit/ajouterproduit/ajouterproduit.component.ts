import { Component, OnInit } from '@angular/core';
import { ServiceproduitService } from 'src/app/core/service/service Produit/serviceproduit.service';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { MatDialogRef, MatSnackBar } from '@angular/material';
import { Produit } from 'src/produit';

@Component({
  selector: 'app-ajouterproduit',
  templateUrl: './ajouterproduit.component.html',
  styleUrls: ['./ajouterproduit.component.css']
})
export class AjouterproduitComponent implements OnInit {
  formaproduit:Produit;
  formee:FormGroup;
  constructor(private produitservice : ServiceproduitService ,private Builder:FormBuilder,private _snackBar: MatSnackBar,
    public dialogRef: MatDialogRef<AjouterproduitComponent> )
      { }

      ngOnInit() {
        this.formee=this.Builder.group(
          {
            idp:new FormControl(0),
            nomp:new FormControl(),
            ref:new FormControl(),
            prix:new FormControl(0),
          }
        )
      
        }

        submit(){
    
          this.formaproduit=this.formee.value;
            this.produitservice.postproduit(this.formaproduit).subscribe(res=>{
              this.produitservice.getAllProduit();
              // window.alert('bien ajoute')
              this._snackBar.open("Produit ajouter","!",
              {
                duration: 2500,
                
              });
              this.dialogRef.close()
              this.formee=this.Builder.group(
                {
                  idp:new FormControl(0),
                  nomp:new FormControl(),
                  ref:new FormControl(),
                  prix:new FormControl(0),
                }
              
              
              )},
            
            
            err=>{
              console.log(err)
              
            })
            
          } 


  onNoClick(): void {
     
    this.dialogRef.close()
  }
}
