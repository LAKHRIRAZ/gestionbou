import { Component, OnInit, Inject } from '@angular/core';
import { ServiceproduitService } from 'src/app/core/service/service Produit/serviceproduit.service';
import { FormBuilder } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef, MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-modifierproduit',
  templateUrl: './modifierproduit.component.html',
  styleUrls: ['./modifierproduit.component.css']
})
export class ModifierproduitComponent implements OnInit {
  formee: any;
  formaproduit: any;
  constructor(private _snackBar: MatSnackBar,private produitservice :ServiceproduitService,private Builder:FormBuilder ,
    @Inject(MAT_DIALOG_DATA) public data:any, public dialogRef: MatDialogRef<ModifierproduitComponent> ) { }


    ngOnInit() {
      console.log(this.data);
      this.formee=this.Builder.group(
        {
          idp:[this.data.idp],
          nomp:[this.data.nomp],
          ref:[this.data.ref],
          prix:[this.data.prix],
         
        }
       
      )
      console.log(this.data.idp);
    }
    get f(){
      return this.formee.controls
    }
      onNoClick(): void {
     
        this.dialogRef.close()
      }
      submit()
      {
        
         this.formaproduit=this.formee.value;
         console.log(this.formaproduit);
         this.produitservice.puttproduit(this.data.idp,this.formaproduit).subscribe(res=>
          {
            this._snackBar.open("Produit Modifier","!",
              {
                duration: 2500,
                
              }); this.onNoClick();
          });
         
      }
  

}
