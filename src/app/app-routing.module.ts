import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListerclientComponent } from './compenent/client/liste client/listerclient/listerclient.component';
import { ListercommandeComponent } from './compenent/commande/liste commande/listercommande/listercommande.component';
import { ListerproduitComponent } from './compenent/produit/liste produit/listerproduit/listerproduit.component';
import { ListeruserComponent } from './compenent/user/liste user/listeruser/listeruser.component';
import { AuthentificationComponent } from './compenent/auth/authentification/authentification.component';
import { SignupComponent } from './compenent/auth/signup/signup/signup.component';


const routes: Routes = [
 
  {
    path: "signup",
    component: SignupComponent,
    pathMatch: 'full'
  },
  
  {
    path: "signin",
    component: AuthentificationComponent,
    pathMatch: 'full'
  },

  {
    path:'Espace Client',
    component:ListerclientComponent,
     pathMatch:'full'
  
  },

 
  {
    path:'Espace Commonde',
    component:ListercommandeComponent,
     pathMatch:'full'
  
  },

  {
    path:'Espace User',
    component:ListeruserComponent,
     pathMatch:'full'
  
  },
  {
    path:'Espace Produit',
    component:ListerproduitComponent,
     pathMatch:'full'
  
  },
  {
    path:'',
    redirectTo:'',
    pathMatch:'full',
  
  }









];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
