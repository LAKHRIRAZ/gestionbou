import { Injectable } from '@angular/core';

import { HttpClient } from '@angular/common/http';
import { Commande } from 'src/commande';

@Injectable({
  providedIn: 'root'
})
export class CommandeserviceService {

  url:string="http://localhost:51026/api/commandes";
  Commandes:Commande[]
  Commaaandes:Commande[]
  constructor(private http:HttpClient) { }

  getAllCommande()
  {
    return   this.http.get(this.url)
    
  }

   ///ajouter
   postCommande(cmd:Commande)
   {
     return this.http.post(this.url,cmd);
   }
 //// modifier
   puttCommande(id,commande:Commande)
   {
     return this.http.put(`${this.url}/${id}`, commande);
   }
   ///delete
   deleteCommande(id)
   {
     return this.http.delete(this.url+"/"+id);
   }
   getgarentidate(d,d1)
   {
    return this.http.get(`${this.url}`+'/du/'+d+'/au/'+d1 );
   }
  exportToExcel(d,d1) {
    return this.http.get(`${this.url}`+ '/export/'+d+'/'+d1);
  }
  // exportToExcel(qte) {
  //   return this.http.get(`${this.url}`+qte);
  // }

}
