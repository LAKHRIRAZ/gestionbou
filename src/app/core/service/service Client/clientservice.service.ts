import { Injectable } from '@angular/core';

import { HttpClient } from '@angular/common/http';
import { Client } from 'src/client';

@Injectable({
  providedIn: 'root'
})
export class ClientserviceService {

  url:string="http://localhost:51026/api/clients";
  url1:string="http://localhost:51026/api/commandes";
  clients:Client[];
  cliient:Client;
  constructor(private http:HttpClient) { }
  getAllClient()
  {
    return   this.http.get(this.url)
    
  }
  ///ajouter
  postclient(cmd:Client)
  {
    return this.http.post(this.url,cmd);
  }
//// modifier
  puttclient(id,client:Client)
  {
    return this.http.put(`${this.url}/${id}`, client);
  }
  ///delete
  deleteclient(id)
  {
    return this.http.delete(this.url+"/"+id);
  }
  getgarentidate(id)
  {
   return this.http.get(`${this.url}`+'/du/'+id);
 
  }
  getcommande( id)
  {
    return this.http.get(`${this.url1}`+ '/commadepar' +'/'+id);
  }
}
