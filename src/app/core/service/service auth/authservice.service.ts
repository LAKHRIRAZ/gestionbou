import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AuthserviceService {
  url:string="http://localhost:51026/api/users";

  constructor(private http:HttpClient) { }


  getUser(username: string, password: string) {
    return this.http.get(`${this.url}` + '/userexists/' + username + '/' + password);
  }
////
lastLogin() {
  return this.http.get('http://localhost:51026/api/logins/lastLogin');
}
 
}
