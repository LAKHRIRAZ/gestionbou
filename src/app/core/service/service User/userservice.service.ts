import { Injectable } from '@angular/core';

import { HttpClient } from '@angular/common/http';
import { User } from 'src/user';

@Injectable({
  providedIn: 'root'
})
export class UserserviceService {

  url:string="http://localhost:51026/api/users";

  Users:User[]
  Ussser:User[]

  constructor(private http:HttpClient) { }

  getAllUsers()
  {
    return this.http.get(this.url)
    
  }
  ///ajouter
  postUsers(cmd:User)
  {
    return this.http.post(this.url,cmd);
  }
//// modifier
  puttUsers(id,user:User)
  {
    return this.http.put(`${this.url}/${id}`, user);
  }
  ///delete
  deleteUsers(id)
  {
    
    return this.http.delete(this.url+"/"+id);
  }
  
}
