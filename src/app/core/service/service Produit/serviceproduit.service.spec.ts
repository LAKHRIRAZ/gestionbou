import { TestBed } from '@angular/core/testing';

import { ServiceproduitService } from './serviceproduit.service';

describe('ServiceproduitService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ServiceproduitService = TestBed.get(ServiceproduitService);
    expect(service).toBeTruthy();
  });
});
