import { Injectable } from '@angular/core';

import { HttpClient } from '@angular/common/http';
import { Produit } from 'src/produit';

@Injectable({
  providedIn: 'root'
})
export class ServiceproduitService {

  url:string="http://localhost:51026/api/produits";
  Produits:Produit[];

  Produuuit:Produit;
  constructor(private http:HttpClient) { }
  getAllProduit()
  {
    return   this.http.get(this.url)
    
  }
   ///ajouter
   postproduit(cmd:Produit)
   {
     return this.http.post(this.url,cmd);
   }
   //// modifier
  puttproduit(id,Produit:Produit)
  {
    return this.http.put(`${this.url}/${id}`, Produit);
  }
    ///delete
    deleteproduit(id)
    {
      return this.http.delete(this.url+"/"+id);
    }
}
