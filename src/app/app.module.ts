import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AjouterclientComponent } from './compenent/client/add client/ajouterclient/ajouterclient.component';
import { ListerclientComponent } from './compenent/client/liste client/listerclient/listerclient.component';
import { ModifierclientComponent } from './compenent/client/modifier client/modifierclient/modifierclient.component';
import { AjoutercommandeComponent } from './compenent/commande/add commande/ajoutercommande/ajoutercommande.component';
import { ListercommandeComponent } from './compenent/commande/liste commande/listercommande/listercommande.component';
import { ModifiercommandeComponent } from './compenent/commande/modifier commande/modifiercommande/modifiercommande.component';
import { AjouterproduitComponent } from './compenent/produit/add produit/ajouterproduit/ajouterproduit.component';
import { ListerproduitComponent } from './compenent/produit/liste produit/listerproduit/listerproduit.component';
import { ModifierproduitComponent } from './compenent/produit/modifier produit/modifierproduit/modifierproduit.component';
import { ListeruserComponent } from './compenent/user/liste user/listeruser/listeruser.component';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { MatToolbarModule, MatInputModule, MatTableModule, MatDialogModule, MatIconModule, MatPaginatorModule, MatNativeDateModule,MatCardModule } from '@angular/material/';
import {MatButtonModule} from '@angular/material/button';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatDatepickerModule} from '@angular/material/datepicker';
import { NotificationComponent } from './compenent/notificattion/notification/notification.component';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import { AjouteruserComponent } from './compenent/user/add user/ajouteruser/ajouteruser.component';
import { ModifieruserComponent } from './compenent/user/modifier user/modifieruser/modifieruser.component';
import { AuthentificationComponent } from './compenent/auth/authentification/authentification.component';
import { ListerComponent } from './compenent/client/lister/lister.component';
import { ImprimeruserComponent } from './compenent/user/imprimer user/imprimeruser/imprimeruser.component';

import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import { SignupComponent } from './compenent/auth/signup/signup/signup.component';


@NgModule({
  declarations: [
    AppComponent,
    AjouterclientComponent,
    ListerclientComponent,
    ModifierclientComponent,
    AjoutercommandeComponent,
    ListercommandeComponent,
    ModifiercommandeComponent,
    AjouterproduitComponent,
    ListerproduitComponent,
    ModifierproduitComponent,
    ListeruserComponent,
    NotificationComponent,
    AjouteruserComponent,
    ModifieruserComponent,
    AuthentificationComponent,
    ListerComponent,
    ImprimeruserComponent,
    SignupComponent,

  
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MatToolbarModule,
    MatInputModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MatTableModule,
    MatButtonModule,
    FormsModule,ReactiveFormsModule,
    MatDialogModule,
    MatIconModule,
    MatFormFieldModule,
    MatPaginatorModule,
    MatAutocompleteModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatNativeDateModule,MatSnackBarModule,
    MatCardModule,
    MatProgressSpinnerModule

  

  ],
  providers: [],
  bootstrap: [AppComponent],
  entryComponents : [AjouterclientComponent,ModifierclientComponent,AjouterproduitComponent,AjoutercommandeComponent,ModifiercommandeComponent,ModifierproduitComponent,AjouteruserComponent,ModifieruserComponent,ListerComponent,ImprimeruserComponent]
})
export class AppModule { }
